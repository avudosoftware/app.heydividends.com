import React, { useState, createContext } from 'react';

interface Notification {
    type: 'error' | 'success' | 'warning' | 'info',
    title: string;
    message: string;
}

interface INotificationContext {
  notification: Notification;
  showNotification: boolean;
  displayNotification: (notification: Notification) => void;
  closeNotification: () => void;
}

export const NotificationContext = createContext({} as INotificationContext);

export const NotificationProvider = ({ children }: any) => {
  const [notification, setNotification] = useState({} as Notification);
  const [showNotification, setShowNotification] = useState(false)

  const displayNotification = (notification: Notification) => {
    setNotification(notification)
    setShowNotification(true)
  }


  // TODO: Find more elegant way of preventing component from rendering until auth is set.
  return (
    <NotificationContext.Provider value={{ notification, showNotification, displayNotification, closeNotification: () => setShowNotification(false) }}>
      {children}
    </NotificationContext.Provider>
  )
}
