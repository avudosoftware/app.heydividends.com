import firebase from 'firebase';
import React, { useEffect, useState, createContext } from 'react';
import { auth } from '../firebase/firebase';

interface AuthContextI {
  currentUser: firebase.User | any;
  // setCurrentUser: (value: any) => void;
}

export const AuthContext = createContext({} as AuthContextI);

export const AuthProvider = ({ children }: any) => {
  const [currentUser, setCurrentUser] = useState(auth.currentUser);
  const [authSet, setAuthSet] = useState(false);

  const pushUserToState = (user: any) => {
    setCurrentUser(user)
    setAuthSet(true)
  }

  useEffect(() => {
    auth.onAuthStateChanged(pushUserToState as any)
    // Fetch user info
  }, [setCurrentUser])

  // TODO: Find more elegant way of preventing component from rendering until auth is set.
  return (
    <AuthContext.Provider value={{ currentUser }}>
      {authSet && children}
    </AuthContext.Provider>
  )
}
