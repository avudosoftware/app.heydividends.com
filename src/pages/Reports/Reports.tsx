import React from 'react';
import ReportsView from '../../components/Views/ReportsView/ReportsView';
import { useGetReports } from '../../api';

const Reports = () => {
  const { isLoading, data } = useGetReports();

  return (
    <ReportsView 
      isLoading={isLoading}
      reports={data}
    />
  )
}

export default Reports;