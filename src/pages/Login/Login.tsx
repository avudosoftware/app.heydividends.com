import React from 'react';
import AuthLayout from '../../authlayout/AuthLayout';
import LoginView from '../../components/Views/LoginView/LoginView';

const Login = () => (
  <AuthLayout>
    <LoginView />
  </AuthLayout>
)

export default Login;