import React from 'react';
import { useParams } from 'react-router-dom';
import ViewReportView from '../../components/Views/ViewReportView/ViewReportView';

const ViewReport = () => {
  const params = useParams(); 
  const { id } = params as any;

  return (
    <ViewReportView 
      reportId={id}
    />
  )
}

export default ViewReport;