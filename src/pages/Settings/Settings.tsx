import { useContext } from 'react';
import { useGetMembership } from '../../api';
import SettingsView from '../../components/Views/SettingsView/SettingsView';
import { AuthContext } from '../../contexts/auth';

const Settings = () => {
  const { currentUser } = useContext(AuthContext);
  const { isLoading, data } = useGetMembership(currentUser?.uid)  

  return (
    <SettingsView
      isLoading={isLoading}
      membership={data}
      userInfo={{
        email: currentUser.email,
        name: currentUser.displayName,
        photoURL: currentUser.photoURL
      }}
    />
  )
}

export default Settings;