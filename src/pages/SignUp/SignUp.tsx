import React from 'react';
import AuthLayout from '../../authlayout/AuthLayout';
import SignUpView from '../../components/Views/SignUpView/SignUpView';

const SignUp = () => (
  <AuthLayout>
    <SignUpView />
  </AuthLayout>
)

export default SignUp;