import React from 'react';
import AuthLayout from '../../authlayout/AuthLayout';
import SubscriptionView from '../../components/Views/SubscriptionView/SubscriptionView';

const Subscribe = () => (
    <AuthLayout>
        <SubscriptionView />
    </AuthLayout>
)

export default Subscribe;