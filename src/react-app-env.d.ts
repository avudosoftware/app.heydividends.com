/// <reference types="react-scripts" />
declare module '@editorjs/embed'
declare module '@editorjs/table'
declare module '@editorjs/paragraph'
declare module '@editorjs/list'
declare module '@editorjs/warning'
declare module '@editorjs/quote'
declare module '@editorjs/simple-image'
declare module '@editorjs/image'
declare module 'editorjs-social-post-plugin';