import { Box, Flex, Image, Skeleton, Text } from '@chakra-ui/react';
import { Author } from '../../interfaces';
import React from 'react';
import { Avatar, Panel } from '../UI';
import './style.css';
import { useHistory } from 'react-router-dom';
import { isMobile } from 'react-device-detect';
import LazyLoad from 'react-lazyload';

interface ReportProps {
  id: string;
  date: string;
  title: string;
  coverImgUrl: string;
  author: Author;
}

const Report = ({ id, date, title, coverImgUrl, author: { avatarImgUrl, name } }: ReportProps) => {
  const history = useHistory()

  return (
    <Panel className='report' style={{ minHeight: 330, padding: 20, cursor: 'pointer', marginBottom: isMobile ? 15 : 0 }} onClick={() => history.push(`/view-report/${id}`)} >
      <Box>
        <Text style={{ fontWeight: 500, color: '#7D7D7D', fontSize: 18 }}>{date}</Text>
        <Text style={{ fontSize: 18, color: '#2D3748', fontWeight: 600, marginBottom: 5 }}>{title}</Text>
        <LazyLoad placeholder={<Skeleton />}>
          <Image alt={title} src={coverImgUrl} style={{ borderRadius: 7, height: 153, maxHeight: 153, width: '100%' }} />
        </LazyLoad>
        <Flex style={{ alignItems: 'center' }}>
          <Avatar style={{ marginLeft: 0 }} avatarImgUrl={avatarImgUrl} />
          <Box>
            <Text style={{ fontWeight: 600, color: '#2D3748', lineHeight: 1 }}>Author</Text>
            <Text style={{ fontWeight: 500, fontSize: 18, color: '#7D7D7D' }}>{name}</Text>
          </Box>
        </Flex>
      </Box>
    </Panel>
  )
}

export default Report;