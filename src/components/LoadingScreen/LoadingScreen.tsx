import React from 'react';
import { Flex, Spinner } from '@chakra-ui/react';

const LoadingScreen = () => (
    <Flex w='100%' h='100vh' style={{justifyContent: 'center', alignItems: 'center'}}>
        <Spinner size='xl' colorScheme='brand' />
    </Flex>
)

export default LoadingScreen;