import React from 'react';
import { Box } from '@chakra-ui/react';
import CreatePost from './CreatePost/CreatePost';

const Feed = () => (
    <Box
        style={{
            width: '100%',
            maxWidth: 650
        }}
    >
        <CreatePost />
    </Box>
)

export default Feed;