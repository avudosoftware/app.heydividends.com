import React from 'react';
import { MentionInput } from '../../UI';
import { Box, Button, Flex } from '@chakra-ui/react';
import { Formik } from 'formik';

const CreatePost = () => (
  <Formik
    initialValues={{ content: '' }}
    onSubmit={() => console.log('hello')}
  >
    {({
      values,
      errors,
      touched,
      handleChange,
      handleBlur,
      handleSubmit,
      setFieldValue
    }) => (
      <form
        onSubmit={(e) => {
          e.preventDefault()
          handleSubmit(e)
        }}
      >
        <Box
          style={{
            backgroundColor: '#F6F6F6',
            padding: 20,
            borderRadius: 8
          }}
        >
          <MentionInput />
          <Flex
            style={{
              justifyContent: 'flex-end',
              marginTop: 15
            }}
          >
            <Button 
            style={{
              width: '100%',
              maxWidth: 100,
              height: 40
            }}
            >
              Post
          </Button>
          </Flex>
        </Box>
      </form>
    )}
  </Formik>
)

export default CreatePost;