import React from 'react';
import { Box, Flex, Spinner, Text } from '@chakra-ui/react';
import { Membership, UserProfile } from '../../../interfaces';
import SettingsForm from '../../Forms/SettingsForm/SettingsForm';

interface SettingsViewProps {
  userInfo: UserProfile;
  membership: Membership;
  isLoading: boolean;
}

const SettingsView = ({ userInfo, isLoading, membership }: SettingsViewProps) => (
  <Flex style={{
    width: '100%',
    justifyContent: 'center'
  }}>
    {isLoading ? (
      <Spinner />
    ) : (
        <Box>
          <Box style={{
            marginBottom: 30
          }}>
            <Text
              style={{
                color: '#333333',
                fontWeight: 700,
                fontSize: 18,
                letterSpacing: 2
              }}
            >
              Settings & Details
            </Text>
            <Text
              style={{
                color: '#333333',
                fontSize: 14,
              }}
            >
              Adjust settings and amend your payment <br></br>details below.
            </Text>
          </Box>
          <SettingsForm
            userInfo={userInfo}
            membership={membership}
          />
        </Box>
      )}
  </Flex>
)

export default SettingsView;