import { Box, Flex, MenuButton, Spinner, Text, Button, Image, MenuItem, Menu, MenuList, useToast } from '@chakra-ui/react';
import React from 'react';
import Editor from 'react-editor-js';
import KeyMetrics from './KeyMetrics/KeyMetrics';
import PriceInfo from './PriceInfo/PriceInfo';
import FilterIcon from '../../../assets/filterIcon.svg';
import { useGetReport } from '../../../api';
import { ReportTitle } from '../../UI';
import CocaColaIcon from '../../../assets/cocaCola.svg';
import { isMobile } from 'react-device-detect';

interface ViewReportViewProps {
  reportId: string;
}

const ViewReportView = ({ reportId }: ViewReportViewProps) => {
  const toast = useToast()
  const { isLoading, error, data } = useGetReport(reportId);
  console.log(data)
  if (isLoading) return <Spinner size='lg' />;
  if (error) {
    toast({
      title: "Error loading report",
      description: 'We failed to get the report, please try another one.',
      status: 'error',
      isClosable: true,
      duration: 3000
    })
  }

  return (
    <Box width='100%'>
      <Flex width='100%'>
        <Box width='100%' style={{ paddingRight: isMobile ? 0 : 50, paddingLeft: isMobile ? 0 : 50 }}>
          <ReportTitle
            title={data && data.title}
            subTitle={data && data.subTitle}
            date={data && data.date}
            companyIcon={CocaColaIcon}
          />
          <Image style={{ borderRadius: 5, marginTop: 25 }} w='100%' src={data.coverImage} />
          {data && data.content && (
            <Editor
              readOnly
              data={data && JSON.parse(data && data?.content)}
            />
          )}
        </Box>
        {!isMobile && (
          <Box>
            <Flex style={{ justifyContent: 'flex-end', marginBottom: 30 }}>
              <Menu placement='bottom-end'>
                <MenuButton as={Button} style={{ background: '#E9F2FF', color: '#7D7D7D', fontSize: 18, fontWeight: 500 }} rightIcon={<Image w='24px' src={FilterIcon} alt='options' />}>
                  Options
              </MenuButton>
                <MenuList>
                  <MenuItem>Downlaod Csv</MenuItem>
                </MenuList>
              </Menu>
            </Flex>
            <Flex style={{ marginBottom: 10, justifyContent: 'flex-end' }}>
              <Text style={{ color: '#2D3748', fontSize: 18, fontWeight: 500 }}>
                Price Info
          </Text>
            </Flex>
            <PriceInfo />
            <Flex style={{ marginBottom: 10, justifyContent: 'flex-end', marginTop: 15 }}>
              <Text style={{ color: '#2D3748', fontSize: 18, fontWeight: 500 }}>
                Key Metrics
          </Text>
            </Flex>
            <KeyMetrics />
          </Box>
        )}
      </Flex>
    </Box>
  )
}

export default ViewReportView;