import React from 'react';
import { Metric, Panel } from '../../../UI';
import { Box } from '@chakra-ui/react';
import DivGrothIcon from '../../../../assets/divGrowth.svg'
import PBIcon from '../../../../assets/pbRatio.svg'
import PEIcon from '../../../../assets/peRatio.svg'
import DivYieldIcon from '../../../../assets/divYield.svg'

const KeyMetrics = () => {
  return (
    <Panel style={{ width: 244 }}>
      <Box style={{ padding: 15 }}>
        <Metric title='Dividend Growth' value='25 Years' icon={DivGrothIcon} />
        <Metric title='P/B Ratio' value='3.45' icon={PBIcon} />
        <Metric title='P/E Ratio' value='12.48' icon={PEIcon} />
        <Metric title='Div Yield' value='3.48%' icon={DivYieldIcon} />
      </Box>
    </Panel>
  )
}

export default KeyMetrics;
