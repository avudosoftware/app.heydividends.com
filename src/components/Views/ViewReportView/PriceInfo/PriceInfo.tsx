import React from 'react';
import { Metric, Panel } from '../../../UI';
import { Box } from '@chakra-ui/react';
import PriceIcon from '../../../../assets/price.svg'


const KeyMetrics = () => {
  return (
    <Panel style={{ width: 244 }}>
      <Box style={{ padding: 15 }}>
        <Metric title='Stock Price' value='$33.49' icon={PriceIcon} />
      </Box>
    </Panel>
  )
}

export default KeyMetrics;
