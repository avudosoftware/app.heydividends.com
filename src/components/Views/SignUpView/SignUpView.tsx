import React from 'react';
import { PageHeading } from '../../UI';
import SignUpForm from '../../Forms/SignUpForm/SignUpForm';

const LoginView = () => (
 <>
  <PageHeading heading='Create An Account' subHeading='Already a member? Login Here' />
  <SignUpForm />
 </>
)

export default LoginView;