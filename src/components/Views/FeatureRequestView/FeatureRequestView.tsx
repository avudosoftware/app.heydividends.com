import React from 'react';
import { Box, Flex, Text } from '@chakra-ui/react';
import FeatureRequestForm from '../../Forms/FeatureRequestForm/FeatureRequestForm';

const FeatureRequestView = () => (
  <Flex style={{
    width: '100%',
    justifyContent: 'center'
  }}>
    <Box>
      <Box style={{
        marginBottom: 30
      }}>
        <Text
          style={{
            color: '#333333',
            fontWeight: 700,
            fontSize: 18,
            letterSpacing: 2
          }}
        >
          Feature Request
            </Text>
        <Text
          style={{
            color: '#333333',
            fontSize: 14,
          }}
        >
         Request a new feature or submit an idea
        </Text>
      </Box>
      <FeatureRequestForm />
    </Box>
  </Flex>
)

export default FeatureRequestView;