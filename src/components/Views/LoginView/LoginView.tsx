import React from 'react';
import { PageHeading } from '../../UI';
import LoginForm from '../../Forms/LoginForm/LoginForm';

const LoginView = () => (
 <>
  <PageHeading heading='Sign In to your account' subHeading='Or create a FREE account' />
  <LoginForm />
 </>
)

export default LoginView;