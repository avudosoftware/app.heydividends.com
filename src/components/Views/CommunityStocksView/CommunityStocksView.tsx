import React from 'react';
import StockSearch from '../../StockSearch/StockSearch';
import Feed from '../../Feed/Feed';
import { Flex, Box } from '@chakra-ui/react';

const CommuityView = () => (
  <Flex
  style={{
    width: '100%'
  }}
  >
    <Box>
      <StockSearch />
    </Box>
    <Flex
      justifyContent='center'
      flexGrow={1}
    >
      <Feed />
    </Flex>
  </Flex>
)

export default CommuityView;