import { Box, Flex, Text, Button, EditablePreview, EditableInput, Editable } from '@chakra-ui/react';
import React, { useState } from 'react';
import moment from 'moment';
import { v4 } from 'uuid';
import Editor from 'react-editor-js';
import { FileUpload, ReportTitle } from '../../UI';
import KeyMetrics from './KeyMetrics/KeyMetrics';
import ThumbnailUpload from './ThumbnailUpload/ThumbnailUpload';

import Embed from '@editorjs/embed'
import Table from '@editorjs/table'
import Paragraph from '@editorjs/paragraph'
import List from '@editorjs/list'
import Warning from '@editorjs/warning'
import Image from '@editorjs/image'
import Quote from '@editorjs/quote'
import SimpleImage from '@editorjs/simple-image'
import SocialPost from 'editorjs-social-post-plugin';

import { storage } from '../../../firebase/firebase';
import { useCreateReport } from '../../../api';


export const EDITOR_JS_TOOLS = {
  embed: {
    class: Embed,
    inlineToolbar: true
  },
  table: Table,
  paragraph: Paragraph,
  list: List,
  warning: Warning,
  image: {
    class: Image,
    config: {
      uploader: {
        async uploadByFile(file: File) {
          var storageRef = storage.ref();
          var imagesRef = storageRef.child('EditorJS').child('images/' + file.name);
          var metadata = {
            contentType: 'image/jpeg'
          };
          var uploadTask = await imagesRef.put(file, metadata);
          const downloadURL = await uploadTask.ref.getDownloadURL();
          return {
            success: 1,
            file: {
              url: downloadURL
            }
          }
        }
      }
    }
  },
  quote: Quote,
  simpleImage: SimpleImage,
  socialPost: SocialPost
}

const WriteReportView = () => {
  const { isLoading, createReport } = useCreateReport()
  const [reportConfig, setReportConfig] = useState({
    id: `company-${v4()}`,
    company: '',
    companyIcon: '',
    coverImage: '',
    thumbNailImg: '',
    title: '',
    subTitle: '',
    date: moment().format('MMM Do YYYY'),
    metrics: {
      divYield: '',
      growth: '',
      pbRatio: '',
      peRatio: '',
    },
    content: {
      version: "2.12.4",
      time: 1556098174501,
      blocks: [
        {
          type: "paragraph",
          data: {
            text:
              "Start typing out the main dish..."
          }
        },
      ]
    }
  })

  return (
    <Flex width='100%'>
      <Flex width='100%'>
        <Box width='100%' style={{ paddingRight: 50, paddingLeft: 50 }}>
          <ReportTitle
            config={reportConfig}
            onChange={setReportConfig}
            companyIcon={reportConfig.companyIcon}
            date={moment().format('MMM Do YYYY')}
            onFileUpload={(url) => {
              setReportConfig({
                ...reportConfig,
                companyIcon: url
              })
            }}
          />
          <Flex style={{
            borderRadius: 5,
            marginTop: 25,
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: '#E9F2FF',
            cursor: 'pointer',
            height: 345,
            background: `${reportConfig.coverImage ? `url(${reportConfig.coverImage})` : '#E9F2FF'}`,
            backgroundPosition: 'center',
            backgroundRepeat: 'no-repeat',
            backgroundSize: 'contain'
          }}
            w='100%'
          >
            <FileUpload
              onUploaded={(url) => {
                setReportConfig({
                  ...reportConfig,
                  coverImage: url
                })
              }}
            />
          </Flex>
          <Box style={{ marginTop: 15 }}>
            <Editor
              data={reportConfig.content}
              tools={EDITOR_JS_TOOLS}
            />
          </Box>
        </Box>
      </Flex>
      <Box>
        <Flex style={{ marginBottom: 10 }}>
          <Button style={{ width: '100%' }} isLoading={isLoading} isDisabled={isLoading} onClick={() => createReport(reportConfig as any)}>
            Publish
          </Button>
        </Flex>
        <Flex style={{ justifyContent: 'flex-end' }}>
          <Editable defaultValue="Company Name...">
            <EditablePreview />
            <EditableInput
              onChange={(e) => setReportConfig({
                ...reportConfig,
                company: e.target.value
              })}
            />
          </Editable>
        </Flex>
        <Flex style={{ marginBottom: 10, justifyContent: 'flex-end' }}>
          <Text style={{ color: '#2D3748', fontSize: 18, fontWeight: 500 }}>
            Thumbnail Upload
          </Text>
        </Flex>
        <ThumbnailUpload
          thumbnail={reportConfig.thumbNailImg}
          onUploaded={(url) => {
            setReportConfig({
              ...reportConfig,
              thumbNailImg: url
            })
          }}
        />
        <KeyMetrics
          onChange={setReportConfig}
          config={reportConfig}
        />
      </Box>
    </Flex>
  )
}

export default WriteReportView;