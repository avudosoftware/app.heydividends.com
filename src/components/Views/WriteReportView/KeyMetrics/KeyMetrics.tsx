import React from 'react';
import { Metric, Panel } from '../../../UI';
import { Box } from '@chakra-ui/react';
import DivGrothIcon from '../../../../assets/divGrowth.svg'
import PBIcon from '../../../../assets/pbRatio.svg'
import PEIcon from '../../../../assets/peRatio.svg'
import DivYieldIcon from '../../../../assets/divYield.svg'

interface KeyMetricProps {
  onChange: (value: any) => void;
  config: any
}

const KeyMetrics = ({ onChange, config }: KeyMetricProps) => {
  return (
    <Panel style={{ width: 244, marginTop: 15 }}>
      <Box style={{ padding: 15 }}>
        <Metric onChange={onChange} config={config} id='growth' title='Dividend Growth' icon={DivGrothIcon} />
        <Metric onChange={onChange} config={config} id='pbRatio' title='P/B Ratio' icon={PBIcon} />
        <Metric onChange={onChange} config={config} id='peRatio' title='P/E Ratio' icon={PEIcon} />
        <Metric onChange={onChange} config={config} id='divYield' title='Div Yield' icon={DivYieldIcon} />
      </Box>
    </Panel>
  )
}

export default KeyMetrics;
