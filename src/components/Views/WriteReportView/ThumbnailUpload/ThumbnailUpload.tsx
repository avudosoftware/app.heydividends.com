import React from 'react';
import { FileUpload, Panel } from '../../../UI';

interface ThumbnailUploadProps {
  onUploaded: (url: string) => void;
  thumbnail: string;
}

const ThumbnailUpload = ({ onUploaded, thumbnail }: ThumbnailUploadProps) => {
  return (
    <Panel style={{
      width: 244,
      height: 155,
      background: `${thumbnail ? `url(${thumbnail})` : '#E9F2FF'}`, border: thumbnail ? '2px solid #E9F2FF' : '',
      backgroundPosition: 'center',
      backgroundRepeat: 'no-repeat',
      backgroundSize: 'cover'
    }}>
      <FileUpload
        onUploaded={onUploaded}
      />
    </Panel>
  )
}

export default ThumbnailUpload;
