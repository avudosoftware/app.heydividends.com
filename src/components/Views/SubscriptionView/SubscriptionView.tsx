import React, { useContext, useState } from 'react';
import { Box, Flex, Text } from '@chakra-ui/react';
import { PageHeading, SelectInput } from '../../UI';
import StripeCheckout from '../../../components/StripeCheckout/StripeCheckout';
import { Elements } from '@stripe/react-stripe-js';
import { loadStripe } from '@stripe/stripe-js';
import { AuthContext } from '../../../contexts/auth';

const stripePromise = loadStripe('pk_test_51I9TVQHpSQ943pZpjvgGZ9D2suJBBsx6sPpD2CFBsP3NjKyjURHeUhY4e2N9Y4k51Apfkz8ycKRzHcrXP6gVsLhk00k3hiElln') as any;

const SubscriptionView = () => {
  const [selectedMembership, setSelectedMembership] = useState(1)
  const { currentUser } = useContext(AuthContext);

  return (
    <Box width='100%'>
      <PageHeading
        heading='Become a Member'
        subHeading='Unlock everything & become a member'
      />
      <Box style={{ textAlign: 'center', marginTop: 15 }}>
        <Text my={1} color='#A7A7A7'>
          ✅ Unlimited Access to all Reports & Features
      </Text>
        <Text my={1} color='#A7A7A7'>
          💬 Access to the community
      </Text>
        <Text my={1} color='#A7A7A7'>
          🆕 Test new features first
      </Text>
        <Text my={1} color='#A7A7A7'>
          🔥 Cancel anytime
      </Text>
        <Text my={1} color='#A7A7A7'>
          📢 52 New Reports added each year
      </Text>
        <Text my={1} color='#A7A7A7'>
          ⚒️ New Features Added Every Month
      </Text>
      </Box>
      <Box style={{ marginTop: 40 }}>
        <Flex>
          <Text style={{ color: '#2D3748', fontWeight: 700, fontSize: 18 }}>Membership:</Text>
        </Flex>
        <SelectInput
          isSelected={selectedMembership === 1}
          title='$4.99'
          subTitle='Monthly Subscription'
          onClick={() => setSelectedMembership(1)}
        />
        <SelectInput
          isSelected={selectedMembership === 2}
          title='$49.99'
          subTitle='Yearly Subscription'
          onClick={() => setSelectedMembership(2)}
        />
      </Box>
      <Box style={{ marginTop: 20 }}>
        <Elements stripe={stripePromise}>
          <StripeCheckout 
            email={currentUser.email}
            isError={false}
            membership={selectedMembership}
          />
        </Elements>
      </Box>
    </Box>
  )
}

export default SubscriptionView;