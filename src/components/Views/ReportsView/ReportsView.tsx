import React from 'react';
import { Flex, Text, SimpleGrid, Box, Spinner } from '@chakra-ui/react';
import Report from '../../../components/Report/Report';
import { ReportListItem } from '../../../interfaces';
import { isMobile } from 'react-device-detect';

interface ReportsViewProps {
  reports: ReportListItem[]
  isLoading: boolean;
}

const ReportsView = ({ reports, isLoading }: ReportsViewProps) => (
  <Box width='100%'>
    <Flex
      width='100%'
      height='40px'
      alignItems='center'
    >
      <Text
        fontSize={18}
        fontWeight={700}
        letterSpacing={2.5}
        color='#2D3748'
        flexGrow={1}
      >
        Reports
        </Text>
    </Flex>
    <Flex width='100%' style={{ marginTop: 10 }}>
      {isLoading ? (
        <Flex width='100%' justifyContent='center' alignItems='center'>
          <Spinner size='lg' />
        </Flex>
      ) : (
        <SimpleGrid display={isMobile ? 'block' : 'grid'} columns={4} spacingX={'10px'} spacingY='20px'>
          {reports && reports.map(r => (
            <Report
              key={r.reportId}
              id={r.reportId}
              date={r.date}
              title={r.title}
              coverImgUrl={r.thumbNailImg}
              author={{
                name: r.author && r.author.name,
                avatarImgUrl: r.author && r.author.avatarImgUrl
              }}
            />
          ))}
        </SimpleGrid>
      )}
    </Flex>
  </Box>
)

export default ReportsView;