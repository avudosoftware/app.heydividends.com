import React from 'react';
import { Flex, Image, Text } from '@chakra-ui/react';
import { Panel } from '../../UI';
import CommunityImage from '../../../assets/coffeeguy.svg';

const CommuityView = () => (
 <>
    <Flex style={{justifyContent: 'center', marginTop: 50, width: '100%'}}>
        <Panel style={{textAlign: 'center', padding: 75, paddingBottom: 20, height: 400}} >
            <Flex style={{justifyContent: 'center'}}>
            <Image maxW={300} alt='guy working' src={CommunityImage} />
            </Flex>
            <Text style={{fontWeight: 800, marginTop: 15}}>Coming Soon</Text>
            <Text fontWeight={500}>The team & I are working hard to build this feature.</Text>
        </Panel>
    </Flex>
 </>
)

export default CommuityView;