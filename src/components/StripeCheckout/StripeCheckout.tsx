import React, { useState } from 'react';
import { CardElement, useElements, useStripe } from '@stripe/react-stripe-js';
import { Text, Flex, Button, useToast } from '@chakra-ui/react';
import './style.css';
import { functions } from '../../firebase/firebase';
import { useHistory } from 'react-router-dom';

interface StripeCheckoutProps {
  email: string;
  isError: boolean;
  membership: number;
}

const StripeCheckout = ({ email, isError, membership }: StripeCheckoutProps) => {
  const [isLoading, setIsLoading] = useState(false);
  const toast = useToast()
  const stripe = useStripe()
  const history = useHistory()
  const elements = useElements()
  const createOptions = () => {
    return {
      style: {
        base: {
          fontSize: '16px',
          color: '#424770',
          fontFamily: 'Open Sans, sans-serif',
          letterSpacing: '0.025em',
          '::placeholder': {
            color: '#aab7c4',
          },
        },
        invalid: {
          color: '#c23d4b',
        },
      }
    }
  };

  const handleSubmit = async () => {
    if(!membership) return;
    if(!stripe || !elements) return;
    setIsLoading(true);

    const result = await stripe?.createPaymentMethod({
      type: 'card',
      card: elements.getElement(CardElement) as any,
      billing_details: {
        email,
      }
    })

    const createSubscription = functions.httpsCallable('createSubscription');
    createSubscription({ email, membership, paymentMethod: result.paymentMethod?.id })
      .then(() => {
        toast({
          status: "success",
          title: 'Welcome! You are now a member.',
          description: `Payment has been processed, you are on the ${membership === 1 ? 'Monthly' : 'Yearly'} plan.`,
          duration: 4000
        })
        setIsLoading(false)
        history.push('/')
      }).catch(e => {
        toast({
          status: 'error',
          title: 'There was an issue when setting you up.',
          description: e
        })
        setIsLoading(false)
      })
  }

  return (
    <form>
      <Flex>
        <Text style={{ color: '#2D3748', fontWeight: 700, fontSize: 18 }}>Payment Details:  </Text>
      </Flex>
      <CardElement
        {...createOptions()}
        className='stripe-main-heydividends'
      />  
      <Button style={{width: '100%'}} isLoading={isLoading} isDisabled={isLoading} onClick={handleSubmit}>
        Subscribe
      </Button>
      <Text my={2} color='#A7A7A7'>
          🔒 Secured over HTTPs & Stripe
      </Text>
    </form>
  )
}

export default StripeCheckout;