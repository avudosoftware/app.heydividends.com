import React from 'react';
import { Menu, MenuList, MenuItem, MenuButton, Image } from '@chakra-ui/react';
import DownArrow from '../../assets/down.svg';
import { auth } from '../../firebase/firebase';
import { useHistory } from 'react-router-dom';

const SettingsDropDown = () => {
  const history = useHistory();

  const onLogOut = async () => {
    try {
      await auth.signOut()
      history.push('/login')
    } catch (e) {
      // TODO: Add error pop up
    }
  }

  const onSettings = () => history.push('/settings')

  return (
    <div>
      <Menu placement='bottom-end'>
        <MenuButton
          py={2}
          transition="all 0.2s"
          borderRadius="md"
          borderWidth="1px"
          border='none'
          _hover={{ bg: "gray.100" }}
          // _expanded={{ bg: "red.200" }}
          _focus={{ outline: 0, boxShadow: "outline" }}
        >
          <Image src={DownArrow} />
        </MenuButton>
        <MenuList marginTop={2}>
          <MenuItem onClick={() => onSettings()}>Settings</MenuItem>
          <MenuItem onClick={async () => onLogOut()}>Log Out</MenuItem>
        </MenuList>
      </Menu>
    </div>
  )
}

export default SettingsDropDown;
