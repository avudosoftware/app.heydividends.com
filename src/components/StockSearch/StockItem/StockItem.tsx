import React from 'react';
import { Flex, Image, Text, Box } from '@chakra-ui/react';

interface StockItemProps {
  logo: string;
  company: string;
  moto: string;
  active?: boolean;
}

const StockItem = ({ logo, company, moto, active }: StockItemProps) => (
  <Box
    style={{
      background: active ? 'linear-gradient(to bottom, rgba(249, 99, 124, 0.8), rgba(255, 121, 51, 0.8))' : 'inherit' ,
      paddingRight: active ? 10 : 0,
      borderBottomLeftRadius: active ? 10 : 0,
      borderTopLeftRadius: active ? 10 : 0,
      cursor: !active ? 'pointer' : 'inherit'
    }}
  >
    <Flex
      style={{
        height: 80,
        alignItems: 'center',
        padding: 15,
        backgroundColor: active ? '#E8E8E8' : '#F4F4F4',
        borderTopLeftRadius: 8,
        borderBottomLeftRadius: 8,
      }}
      _hover={{
        backgroundColor: '#E8E8E8 !important'
      }}
    >
      <Image
        src={logo}
        alt={company}
        style={{
          width: 42,
          height: 42
        }}
      />
      <Box style={{
        marginLeft: 10
      }}>
        <Text
          style={{
            fontSize: 14,
            fontWeight: 600,
            color: '#2D3748'
          }}
        >
          {company}
        </Text>
        <Text
          style={{
            color: 'rgba(125, 125, 125, 0.6)',
            fontSize: 14,
            fontWeight: 400,
          }}
        >
          {moto}
        </Text>
      </Box>
    </Flex>
  </Box>
)

export default StockItem;