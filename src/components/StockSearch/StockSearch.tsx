import React from 'react';
import { Box } from '@chakra-ui/react';
import { SearchInput } from '../UI';
import StockItem from './StockItem/StockItem';
import CocaColaLogo from '../../assets/cocaCola.svg';
import './style.css';

const StockSearch = () => (
  <Box style={{
    height: '100%',
    maxHeight: 700,
    backgroundColor: '#F4F4F4',
    borderRadius: 8,
    overflowX: 'hidden',
  }}
  className='stockSearch'
  >
    <Box
      style={{
        padding: 10
      }}
    >
      <SearchInput
        placeholder='Search stocks...'
      />
    </Box>
    <Box>
      <StockItem 
        company='Coca Cola'
        moto='Better than pepsi'
        logo={CocaColaLogo}
      />
      <StockItem 
        company='Coca Cola'
        moto='Better than pepsi'
        logo={CocaColaLogo}
        active
      />
       <StockItem 
        company='Coca Cola'
        moto='Better than pepsi'
        logo={CocaColaLogo}
      />
       <StockItem 
        company='Coca Cola'
        moto='Better than pepsi'
        logo={CocaColaLogo}
      />
       <StockItem 
        company='Coca Cola'
        moto='Better than pepsi'
        logo={CocaColaLogo}
      />
       <StockItem 
        company='Coca Cola'
        moto='Better than pepsi'
        logo={CocaColaLogo}
      />
       <StockItem 
        company='Coca Cola'
        moto='Better than pepsi'
        logo={CocaColaLogo}
      />
       <StockItem 
        company='Coca Cola'
        moto='Better than pepsi'
        logo={CocaColaLogo}
      />
       <StockItem 
        company='Coca Cola'
        moto='Better than pepsi'
        logo={CocaColaLogo}
      />
       <StockItem 
        company='Coca Cola'
        moto='Better than pepsi'
        logo={CocaColaLogo}
      />
       <StockItem 
        company='Coca Cola'
        moto='Better than pepsi'
        logo={CocaColaLogo}
      />
    </Box>
  </Box>
)

export default StockSearch;