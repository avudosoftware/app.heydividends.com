import { Flex, Box, Image, Text, Editable, EditablePreview, EditableInput } from '@chakra-ui/react';
import React from 'react';

interface MetricProps {
  title: string;
  id?: string;
  value?: any;
  icon: string;
  style?: React.CSSProperties
  onChange?: (value: any) => void;
  config?: any;
  onBlur?: () => void;
  name?: string;
}

const Metric = ({ title, value, icon, style, onChange, onBlur, name, config, id }: MetricProps) => (
  <Flex key={title} style={{ marginTop: 10, marginBottom: 10, ...style }}>
    <Image src={icon} alt={title} />
    <Box style={{ marginLeft: '1rem' }}>
      <Text style={{ color: '#2D3748', fontSize: 14, fontWeight: 600 }}>{title}</Text>
      <Text style={{ color: 'rgba(#7D7D7D, 0.6)', fontSize: 14, fontWeight: 400 }}>
        {!value && onChange ? (
          <Editable defaultValue="Click & Enter Value...">
            <EditablePreview />
            <EditableInput onChange={(e) => onChange({
              ...config,
              metrics: {
                ...config.metrics,
                [id as string]: e.target.value
              }
            })} onBlur={onBlur} name={name} />
          </Editable>
        ) : (value)}
      </Text>
    </Box>
  </Flex>
)

export default Metric;