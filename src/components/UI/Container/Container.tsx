import React from "react"
import { Flex, BoxProps } from "@chakra-ui/react"

export const Container = (props: BoxProps) => (
  <Flex
    w="full"
    pb="12"
    pt="3"
    mx="auto"
    maxW="93.75rem"
    px={{ base: "2", md: "6" }}
    {...props}
  />
)

export default Container;