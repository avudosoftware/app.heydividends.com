import React from 'react';
import { Box, Flex, Image, Text } from '@chakra-ui/react';
import RadioDefaultIcon from '../../../assets/radioUnselected.svg';
import RadioDefaultSelectedIcon from '../../../assets/radioSelected.svg';


interface SelectInputProps {
  title: string;
  subTitle: string;
  isSelected: boolean;
  isCancelable?: boolean;
  onClick: () => void;
  cancelSubscription?: () => void;
}

const SelectInput = ({ title, subTitle, isSelected, isCancelable, onClick, cancelSubscription }: SelectInputProps) => (
  <Flex my={2} style={{ alignItems: 'center', backgroundColor: '#F4F4F4', borderRadius: 8, cursor: isCancelable ? 'inherit' : 'pointer', border: (isSelected && '3px solid #1ABC9C') || '', padding: 15, height: 80 }} onClick={() => onClick()} >
    <Image style={{ marginLeft: 10 }} alt='select' src={isSelected ? RadioDefaultSelectedIcon : RadioDefaultIcon} />
    <Box style={{ marginLeft: 20 }}>
      <Text style={{ fontWeight: 700, fontSize: 18, color: '#7D7D7D' }}>
        {title}
      </Text>
      <Text style={{ fontWeight: 400, fontSize: 18, color: '#7D7D7D' }}>
        {subTitle}
      </Text>
    </Box>
    {isCancelable && (
      <Flex
        style={{
          justifyContent: 'flex-end',
          alignItems: 'center',
          flexGrow: 1
        }}
      >
        <Text
          style={{
            color: '#F94E4E',
            fontSize: 18,
            fontWeight: 700,
            cursor: 'pointer'
          }}
          _hover={{
            borderBottom: 'solid 3px #F94E4E'
          }}
          onClick={cancelSubscription}
        >
          Cancel
         </Text>
      </Flex>
    )}
  </Flex>
)

export default SelectInput;