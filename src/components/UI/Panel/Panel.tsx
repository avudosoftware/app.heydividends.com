import React from 'react';
import { Box } from '@chakra-ui/react';

interface PanelProps {
  children: any;
  [x: string]: any;
} 

const Panel = ({ children, ...props }: PanelProps) => (
    <Box bgColor='#E9F2FF' {...props} borderRadius={8} >
        {children}
    </Box>
)

export default Panel;