import React from 'react';
import { Avatar } from '@chakra-ui/react';
import AvatarIcon from '../../../assets/avatar.svg';
import AvatarBorder from '../../../assets/avatarBorder.svg';

interface AvatarProps {
  avatarImgUrl?: string;
  style?: React.CSSProperties
}

const ChakraAvatar = ({ avatarImgUrl, style }: AvatarProps) => {
  return (
    <div style={{marginLeft: 20, ...style}}>
       <div style={{background: `url(${AvatarBorder})`, padding: 24, backgroundPositionX: 'left', backgroundPositionY: 'bottom', backgroundRepeat: 'no-repeat' }}>
        <Avatar style={{background: 'none'}} w={35} h={35} src={avatarImgUrl || AvatarIcon} />
      </div>
    </div>
  )
}

export default ChakraAvatar;
