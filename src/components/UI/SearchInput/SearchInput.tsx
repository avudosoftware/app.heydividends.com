import React from 'react';
import { Input, InputGroup, InputLeftElement } from '@chakra-ui/react';
import { Search2Icon } from '@chakra-ui/icons';

interface SearchInputProps {
  placeholder: string;
}

const SearchInput = ({ placeholder }: SearchInputProps) => {

  return (
    <InputGroup>
      <InputLeftElement
        pointerEvents="none"
        children={<Search2Icon color="gray.300" />}
      />
      <Input
        placeholder={placeholder}
        _placeholder={{
          color: '#ABABAB'
        }}
        style={{
          backgroundColor: '#E8E8E8'
        }}
      />
    </InputGroup>
  )
}

export default SearchInput;