import React from 'react';
import { Box, Flex, Image, Heading, Text, Editable, EditablePreview, EditableInput } from '@chakra-ui/react';
import FileUpload from '../FileUpload/FileUpload';
import { isMobile } from 'react-device-detect';

interface ReportTitle {
  companyIcon?: string;
  title?: string;
  subTitle?: string;
  date?: string;
  onFileUpload?: (url: string) => void;
  onChange?: (value: any) => void;
  config?: any
}

const Panel = ({ title, subTitle, companyIcon, date, onFileUpload, onChange, config }: ReportTitle) => (
  <Box style={{ textAlign: 'center' }}>
    <Flex justifyContent='center' marginBottom='5px'>
      {companyIcon ? (<Image maxW='50px' height='50px' borderRadius='5px' src={companyIcon} alt={title} />) :
        (
          <Flex
            _hover={{
              backgroundColor: '#E2E8F0'
            }}
            style={{ justifyContent: 'center', alignItems: 'center', width: 50, height: 50, borderRadius: 5, backgroundColor: '#E9F2FF', cursor: 'pointer' }}>
            {onFileUpload && (
              <FileUpload
                onUploaded={onFileUpload}
              />
            )}
          </Flex>
        )}
    </Flex>
    <Heading style={{ color: '#2D3748', marginBottom: 5, fontWeight: 800, fontSize: 28 }} >
      {!title && onChange ? (
        <Editable defaultValue="Report Title...">
          <EditablePreview />
          <EditableInput
            onChange={(e) => onChange({
              ...config,
              title: e.target.value,
            })}
          />
        </Editable>
      ) : (title)}
    </Heading>
    <Text style={{ color: 'rgba(125, 125, 125, 0.58)', fontSize: 14, fontWeight: 500, marginBottom: 5 }}>
      {date}
    </Text>
    <Text style={{ color: '#7D7D7D', fontSize: 14, fontWeight: 400, paddingLeft: isMobile ? 0 : 250, paddingRight: isMobile ? 0 : 250 }}>
      {!subTitle && onChange ? (
        <Editable defaultValue="Add Sub title...">
          <EditablePreview />
          <EditableInput onChange={(e) => onChange({
            ...config,
            subTitle: e.target.value,
          })} />
        </Editable>
      ) : (subTitle)}
    </Text>
  </Box>
)

export default Panel;