import { Flex } from '@chakra-ui/react';
import React from 'react';
import './style.css';

interface DividerProps {
  text: string;
} 

const Divider = ({ text }: DividerProps) => (
    <Flex style={{justifyContent: 'center', fontWeight: 700 }} className='divider'>
        {text}
    </Flex>
)

export default Divider;