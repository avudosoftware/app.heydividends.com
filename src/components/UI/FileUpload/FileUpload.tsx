import { AddIcon } from '@chakra-ui/icons';
import { Spinner, useToast } from '@chakra-ui/react';
import React, { ChangeEvent, useState } from 'react';
import { storage } from '../../../firebase/firebase';
import moment from 'moment';
import './style.css'

interface FileUploadProps {
  onUploaded: (url: string) => void;
  styles?: React.CSSProperties;
  iconWidth?: number;
  iconHeight?: number;
}

const FileUpload = ({ onUploaded, styles, iconHeight, iconWidth }: FileUploadProps) => {
  const hiddenFileInput = React.useRef(null) as any;
  const [isLoading, setIsLoading] = useState(false)
  const toast = useToast()

  const handleClick = (event: any) => {
    event.preventDefault()
    hiddenFileInput.current.click();
  };

  const handleChange = (event: ChangeEvent<HTMLInputElement>) => {
    if (event.target.files) {
      const file = event.target.files[0]
      setIsLoading(true);
      const fileName = `${moment().milliseconds()}-${file.name}`.replace(' ', '')
      const filepath = `imgs/${fileName}`
      const storageRef = storage.ref(filepath)
      const task = storageRef.put(file)
      task.on('state_changed', (snapshot) => {
        // Add percentage
      },
        (err) => {
          toast({
            title: 'Error uploading file',
            description: 'We failed to upload your file, please try again.',
            status: 'error',
            duration: 3000,
            isClosable: true,
          })
        },
        () => {
          onUploaded(`https://firebasestorage.googleapis.com/v0/b/heydividends-a4c68.appspot.com/o/imgs%2F${fileName}?alt=media`)
          setIsLoading(false);
        }
      )
    }
  };

  return (
    <>
      {isLoading ? (
        <Spinner />
      ) : (
          <>
            <input onChange={handleChange} ref={hiddenFileInput} type="file" id="actual-btn" style={{ display: 'none' }} />
            <label onClick={handleClick} className='fileUpload' style={{...styles}} htmlFor="actual-btn">
              <AddIcon color='#7D7D7D' height={iconHeight || 25} width={iconWidth || 25} />
            </label>
          </>
        )}
    </>
  )
}

export default FileUpload;