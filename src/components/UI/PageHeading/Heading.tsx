import React from 'react';
import { Heading, Text } from '@chakra-ui/react';

interface PageHeadingProps {
  heading: string;
  subHeading: string;
} 

const PageHeading = ({ heading, subHeading }: PageHeadingProps) => (
  <div style={{textAlign: 'center'}}>
    <Heading color='brand.700' fontSize='1.95rem'>{heading}</Heading>
    <Text py={1} fontWeight={500} color='brand.400' fontSize={18}>{subHeading}</Text>
  </div>
)

export default PageHeading;