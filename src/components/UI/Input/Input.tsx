import React from 'react';
import { Input } from '@chakra-ui/react';

interface InputProps {
  label: string;
  placeholder: string;
  type: "text" | "password",
  name?: string;
  onChange?: (value: any) => void;
  onBlur?: (e: React.FocusEvent<any>) => void;
  value?: any;
  error?: string;
  touched?: boolean;
  isDisabled?: boolean; 
}

const DefaultInput = ({ label, placeholder, type, name, onChange, onBlur, value, error, touched, isDisabled }: InputProps) => (
  <div style={{marginBottom: '1rem'}}>
    <label style={{fontWeight: 600}} >{label}</label>
    <Input 
      variant='default' 
      border={`2px solid ${(!error && touched ? '#1ABC9C' : '#EDEDED')}`}
      background='#F4F4F4'
      color='#BFBFBF'
      placeholder={placeholder}
      type={type}
      name={name}
      onChange={onChange}
      onBlur={onBlur}
      value={value}
      isDisabled={isDisabled}
    />
    {error && touched && (<p style={{color: '#F94E4E'}}>{error}</p>)}
  </div>
)

export default DefaultInput;