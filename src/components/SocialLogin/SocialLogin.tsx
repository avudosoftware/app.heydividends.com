import React from 'react';
import { Flex, IconButton, Image, useToast } from '@chakra-ui/react';
import GoogleIcon from '../../assets/google.svg';
import TwitterIcon from '../../assets/twitter.svg';
import FacebookIcon from '../../assets/facebook.svg';
import { app, auth, db } from '../../firebase/firebase';
import { useHistory } from 'react-router-dom';

const SocialLogin = () => {
  const toast = useToast()
  const history = useHistory()
  const createProvider = (provider: 'google' | 'facebook' | 'twitter') => {
    if (provider) {
      switch (provider) {
        case 'twitter':
          return new app.auth.TwitterAuthProvider();
        case 'google':
          return new app.auth.GoogleAuthProvider();
        default:
          return new app.auth.FacebookAuthProvider();
      }
    }
  }

  const onSignInWithProvider = async (provider: 'google' | 'facebook' | 'twitter') => {
    try {
      const authProvider = createProvider(provider)
      if (authProvider) {
        const response = await auth.signInWithPopup(authProvider)
        if(response.additionalUserInfo?.isNewUser) {
          await db.collection('users').doc(`user-${response.user?.uid}`).set({
            membership: {
              type: ''
            }
          })
        }
        history.push('/')
        toast({
          title: 'Login Success',
          description: 'Welcome back',
          duration: 4000,
          status: 'success',
          isClosable: true
        })
      }
    } catch (e) {
      // TODO: Notification Popup
      toast({
        title: 'Login Failed',
        description: e.message,
        duration: 4000,
        status: 'error',
        isClosable: true
      })
    }
  }
  return (
    <Flex style={{ justifyContent: 'center' }}>
      <IconButton mx={2} backgroundColor='#F4F4F4' w={20} variant='ghost' onClick={async () => await onSignInWithProvider('google')} aria-label='Google login' icon={<Image src={GoogleIcon} />} />
      <IconButton mx={2} backgroundColor='#F4F4F4' w={20} variant='ghost' onClick={async () => await onSignInWithProvider('twitter')} aria-label='Twitter login' icon={<Image src={TwitterIcon} />} />
      <IconButton mx={2} backgroundColor='#F4F4F4' w={20} variant='ghost' aria-label='Facebook login' onClick={async () => await onSignInWithProvider('facebook')} icon={<Image src={FacebookIcon} />} />
    </Flex>
  )
}

export default SocialLogin;