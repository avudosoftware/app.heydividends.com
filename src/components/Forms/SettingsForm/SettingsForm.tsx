import React from 'react';
import { Formik } from 'formik';
import { Membership, UserProfile } from '../../../interfaces';
import { FileUpload, Input, Panel, SelectInput } from '../../UI';
import { Text, Flex, Button, Box, Image, Spinner } from '@chakra-ui/react';
import { useCancelSubscription, useUpdateUser } from '../../../api';
import MoneyHand from '../../../assets/money.svg'
import SettingsSchema from '../../../validation/SettingsSchema'
import { useHistory } from 'react-router-dom';
import { isMobile } from 'react-device-detect';
import './style.css'

interface SettingsFormProps {
  userInfo: UserProfile;
  membership: Membership;
}

const SettingsView = ({ userInfo, membership }: SettingsFormProps) => {
  const { cancelSubscription, loading } = useCancelSubscription();
  const { isLoading, updateUser } = useUpdateUser();
  const history = useHistory();

  const hasChanged = (initialValues: UserProfile, newValues: UserProfile) => {
    let anyChanged;
    if (initialValues.email !== newValues.email) anyChanged = true;
    if (initialValues.name !== newValues.name) anyChanged = true;
    if (initialValues.photoURL !== newValues.photoURL) anyChanged = true;
    return anyChanged;
  }

  return (
    <Formik
      initialValues={{ name: userInfo.name, email: userInfo.email, avatar: userInfo.photoURL }}
      onSubmit={(v) => updateUser(v, { name: userInfo.name, email: userInfo.email, avatar: userInfo.photoURL }, true)}
      validationSchema={SettingsSchema}
    >
      {({
        values,
        errors,
        touched,
        handleChange,
        handleBlur,
        handleSubmit,
        setFieldValue
      }) => (
        <form
          onSubmit={(e) => {
            e.preventDefault()
            handleSubmit(e)
          }}
          style={{
            minWidth: isMobile ? 'unset' : 450
          }}
        >
          <Flex style={{ marginBottom: 10 }}>
            <Box>
              <label style={{ fontWeight: 600 }} >Avatar</label>
              <Box
                className='avatarSelect'
                style={{
                  width: 50,
                  height: 50,
                  border: '3px solid #EDEDED',
                  borderRadius: '100px',
                  background: `url(${values.avatar})`,
                }}
              >
                <FileUpload
                  onUploaded={(url) => {
                    setFieldValue('avatar', url)
                  }}
                  iconWidth={15}
                  iconHeight={15}
                  styles={{ borderRadius: 100 }}
                />
              </Box>
            </Box>
          </Flex>
          <Input
            name='name'
            value={values.name}
            onChange={handleChange}
            onBlur={handleBlur}
            placeholder='Enter name...'
            label='Name:'
            type='text'
            isDisabled={isLoading}
          />
          <Input
            name='email'
            value={values.email}
            onChange={handleChange}
            onBlur={handleBlur}
            placeholder='Enter email...'
            label='Email:'
            type='text'
            isDisabled={isLoading}
          />
          <Button
            type='submit'
            isLoading={isLoading}
            style={{
              width: '100%',
              marginBottom: 15
            }}
            isDisabled={isLoading || !hasChanged(userInfo, { email: values.email, name: values.name, photoURL: values.avatar })}
          >
            Update
          </Button>
          <Flex>
            <Text style={{ color: '#2D3748', fontWeight: 700, fontSize: 18 }}>Membership:</Text>
          </Flex>
          {membership && membership?.type && (
            <>
              {loading ? (
                <Spinner />
              ) : (
                  <SelectInput
                    title={membership.price}
                    subTitle={`${membership.type} Subscription`}
                    isSelected={true}
                    onClick={() => console.log('click')}
                    isCancelable
                    cancelSubscription={cancelSubscription}
                  />
                )}
            </>
          )}
          {!membership.type && (
            <Panel marginTop={2}>
              <Box textAlign='center' padding={30}>
                <Flex justifyContent='center'>
                  <Image alt='money' src={MoneyHand} />
                </Flex>
                <Text fontSize={18} fontWeight={600}>Access Everything</Text>
                <Text fontSize={14}>
                  View all reports and find potential investment opportunities
               </Text>
                <Button onClick={() => history.push('/subscribe')} _hover={{
                  bg: 'linear-gradient(to left, rgba(249, 99, 124, 0.8), rgba(255, 121, 51, 0.8))',
                }} w='100%' marginTop={5}>Go Premium</Button>
              </Box>
            </Panel>
          )}
        </form>
      )}
    </Formik>
  )
}

export default SettingsView;