import React from 'react';
import { Input, Divider } from '../../UI';
import { Button, Flex, Box, useToast } from '@chakra-ui/react';
import SocialLogin from '../../SocialLogin/SocialLogin';
import { Formik } from 'formik';
import { auth, db } from '../../../firebase/firebase';
import { useHistory } from 'react-router-dom';
import { LoginFormValues } from '../../../interfaces';
import LoginSchema from '../../../validation/LoginSchema'
import { isMobile } from 'react-device-detect';

const LoginForm = () => {
  const history = useHistory(); 
  const toast = useToast();

  const onLogin = async (values: LoginFormValues) => {
    try {
      const user = await auth.signInWithEmailAndPassword(values.email, values.password);
      if(user) {
        await db.collection('users').doc(`user-${user.user?.uid}`).set({
          membership: {
            type: ''
          }
        })
      }
      history.push('/')
      toast({
        title: 'Login Success',
        description: 'Welcome back',
        duration: 4000,
        status: 'success',
        isClosable: true
      })
    } catch (e) {
      toast({
        title: 'Login Failed',
        description: e.message,
        duration: 4000,
        status: 'error',
        isClosable: true
      })
    }
  }

  return (
    <Formik
      initialValues={{email: '', password: ''}}
      onSubmit={async (values) => onLogin(values)}
      validationSchema={LoginSchema}
    >
      {({
        values,
        errors,
        touched,
        handleChange,
        handleBlur,
        handleSubmit,
        isSubmitting,
      }) => (
        <form
          onSubmit={(e) => {
            e.preventDefault();
            handleSubmit(e)
          }}
        >
          <Box style={{ width: isMobile ? 'unset' : 450 }}>
            <div style={{ marginTop: '3rem' }}>
              <Input name='email' value={values.email} onChange={handleChange} onBlur={handleBlur} placeholder='Enter Email...' label='Email:' type='text' error={errors.email} touched={touched.email} />
              <Input name='password' value={values.password} onChange={handleChange} onBlur={handleBlur} placeholder='Enter Password...' label='Password:' type='password' error={errors.password} touched={touched.password} />
            </div>
            <Flex style={{ justifyContent: 'center', marginTop: '2rem' }}>
              <Button w={100} type='submit' isDisabled={isSubmitting} isLoading={isSubmitting}>
                Login
              </Button>
            </Flex>
            <Flex>
              <Divider text='OR' />
            </Flex>
            <Box>
              <SocialLogin />
            </Box>
          </Box>
        </form>
      )}
    </Formik>
  )
}

export default LoginForm;