import React from 'react';
import { useHistory } from "react-router-dom";
// Components
import { Input, Divider } from '../../UI';
import { Button, Flex, Box, Checkbox, Image, useToast } from '@chakra-ui/react';
// Forms
import { Formik } from 'formik';
// Firebase
import { auth } from '../../../firebase/firebase';
// Intercases
import { SignUpFormValues } from '../../../interfaces';
import registrationSchema from '../../../validation/RegistrationSchema';
import SocialLogin from '../../SocialLogin/SocialLogin';
import CheckBoxIcon from '../../../assets/checked.svg';
import { isMobile } from 'react-device-detect';

const SignUpForm = () => {
  const history = useHistory();
  const toast = useToast();

  const onSignUp = async (values: SignUpFormValues) => {
    try {
      await auth.createUserWithEmailAndPassword(values.email, values.password);
      await auth.currentUser?.updateProfile({
        displayName: values.name
      })
  
      history.push('/')
    } catch (e) {
      toast({
        title: 'Error logging in',
        description: e.message,
        duration: 3000,
        status: 'error',
        isClosable: true
      })
    }
  }

  return (
    <Formik
      initialValues={{ email: '', name: '', password: '', repeatPassword: '', tandc: false, tofs: false }}
      onSubmit={ async (values) => await onSignUp(values)}
      validationSchema={registrationSchema}
    >
      {({
        values,
        errors,
        touched,
        handleChange,
        handleBlur,
        handleSubmit,
        isSubmitting,
      }) => (
       <form
        onSubmit={(e) => {
          e.preventDefault()
          handleSubmit(e)
        }}
       >
          <Box style={{ width: isMobile ? 'unset' : 450 }}>
          <Box style={{ marginTop: '3rem' }}>
            <Input placeholder='Enter Name...' label='Name:' type='text' name='name' onBlur={handleBlur} onChange={handleChange} value={values.name} error={errors.name} touched={touched.name} />
            <Input placeholder='Enter Email...' label='Email:' type='text' name='email' onBlur={handleBlur} onChange={handleChange} value={values.email} error={errors.email} touched={touched.email} />
            <Input placeholder='Enter Password...' label='Password:' type='password' name='password' onBlur={handleBlur} onChange={handleChange} value={values.password} error={errors.password} touched={touched.password} />
            <Input placeholder='Enter Password...' label='Repeat Password:' type='password' name='repeatPassword' onBlur={handleBlur} onChange={handleChange} value={values.repeatPassword} error={errors.repeatPassword} touched={touched.repeatPassword}/>
            <Checkbox name='tandc' onBlur={handleBlur} onChange={handleChange} isChecked={values.tandc} colorScheme='brand' icon={<Image src={CheckBoxIcon} />} marginTop={2} color='#A7A7A7'>
              I accept the <span style={{ borderBottom: 'solid 2px #A7A7A7' }}>Terms & Conditions</span>
            </Checkbox>
            <Checkbox name='tofs' onBlur={handleBlur}  isChecked={values.tofs} onChange={handleChange} colorScheme='brand' icon={<Image src={CheckBoxIcon} />} marginTop={2} color='#A7A7A7'>
              I accept the <span style={{ borderBottom: 'solid 2px #A7A7A7' }}>Terms of Service</span>
            </Checkbox>
          </Box>
          <Flex style={{ justifyContent: 'center', marginTop: '2rem' }}>
            <Button type='submit' w={100} isLoading={isSubmitting} isDisabled={isSubmitting}>
              Register
            </Button>
          </Flex>
          <Flex>
            <Divider text='OR' />
          </Flex>
          <Box>
            <SocialLogin />
          </Box>
        </Box>
       </form>
      )}
    </Formik>
  )
}

export default SignUpForm;