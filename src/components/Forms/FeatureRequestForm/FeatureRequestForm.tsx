import React from 'react';
import { Formik } from 'formik';
import { TextArea } from '../../UI';
import { Button, Box } from '@chakra-ui/react';
import FeatureFormSchema from '../../../validation/FeatureFormSchema'
import { useCreateFeatureRequest } from '../../../api';
import { isMobile } from 'react-device-detect';


const FeatureRequestForm = () => {
  const { isLoading, createRequest } = useCreateFeatureRequest()
  const isValid = (initialValue: string, newValue: string) => (initialValue === newValue);

  return (
    <Formik
      initialValues={{ info: '' }}
      onSubmit={(v) => createRequest(v.info)}
      validationSchema={FeatureFormSchema}
    >
      {({
        values,
        handleChange,
        handleBlur,
        handleSubmit,
      }) => (
        <form
          onSubmit={(e) => {
            e.preventDefault()
            handleSubmit(e)
          }}
          style={{
            minWidth: isMobile ? 'unset' : 450
          }}
        >
          <Box style={{ marginBottom: 10 }}>
            <TextArea
              name='info'
              value={values.info}
              onChange={handleChange}
              onBlur={handleBlur}
              placeholder='Enter Feature Idea...'
              label='Feature Request:'
              type='text'
            />
            <Button
              type='submit'
              style={{
                width: '100%',
                marginBottom: 15
              }}
              isLoading={isLoading}
              isDisabled={(isValid('', values.info) || isLoading)}
            >
              Submit
          </Button>
          </Box>
        </form>
      )}
    </Formik>
  )
}

export default FeatureRequestForm;