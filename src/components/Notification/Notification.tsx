import React, { useContext } from 'react';
import { Alert, AlertDescription, AlertIcon, AlertTitle, CloseButton, Flex, Box } from '@chakra-ui/react';
import { NotificationContext } from '../../contexts/notification';


const Notification = () => {
  const { showNotification, closeNotification, notification } = useContext(NotificationContext)

  return (
    <>
      {showNotification && (
        <Flex style={{ position: 'fixed', bottom: 10, width: '100%', justifyContent: 'center' }}>
          <Alert variant="solid" status={notification.type} style={{ borderRadius: 5, maxWidth: 'fit-content' }}>
            <AlertIcon />
            <Box flex='1'>
              <AlertTitle mr={2}>{notification.title}</AlertTitle>
              <AlertDescription>{notification.message}</AlertDescription>
            </Box>
            <Flex style={{alignItems: 'center', marginLeft: 10}}>
              <CloseButton onClick={closeNotification} />
            </Flex>
          </Alert>
        </Flex>
      )}
    </>
  )
}

export default Notification;