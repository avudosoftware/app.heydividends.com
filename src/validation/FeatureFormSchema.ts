import * as yup from 'yup';

const loginSchema = yup.object().shape({
    info: yup.string().required().min(10, 'Must be at least 10 characters long.'),
})

export default loginSchema;