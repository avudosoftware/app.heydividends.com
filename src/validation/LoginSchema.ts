import * as yup from 'yup';

const loginSchema = yup.object().shape({
    email: yup.string().email('Please enter a valid email').required(),
    password: yup.string().required(),
})

export default loginSchema;