import * as yup from 'yup';

const registrationSchema = yup.object().shape({
    email: yup.string().email('Please enter a valid email').required(),
    name: yup.string().min(2).max(50).required(),
    password: yup.string().min(6, 'Your password must be at least 6 Characters long').required(),
    repeatPassword: yup.string().required()
     .oneOf([yup.ref('password'), null], 'Passwords must match'),
    tandc: yup.boolean().required().is([true], 'You must read & accept the Terms & Conditions')
})

export default registrationSchema;