import * as yup from 'yup';

const registrationSchema = yup.object().shape({
    email: yup.string().email('Please enter a valid email').required(),
    name: yup.string().min(2).max(50).required(),
    avatar: yup.string().url()
})

export default registrationSchema;