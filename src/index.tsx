import React, { Suspense } from 'react';
import ReactDOM from 'react-dom';
import { Route, BrowserRouter as Router } from 'react-router-dom';
import { ChakraProvider, CSSReset, extendTheme } from "@chakra-ui/react"
import LoadingScreen from './components/LoadingScreen/LoadingScreen';
import Notification from './components/Notification/Notification';
// Theme Variables
import Fonts from './fonts/Fonts';
import styles from './theme/styles';
import colors from './theme/colours';
import fonts from './theme/fonts';
import button from './theme/components/button';
import input from './theme/components/input';
// Providers
import { AuthProvider } from './contexts/auth';
import { NotificationProvider } from './contexts/notification';
// Routes
import PrivateRoute from './routes/PrivateRoutes';
import routes from './routes/routes';
import authRoutes from './routes/authRoutes';
import './index.css';
import Layout from './layout/Layout';
// import reportWebVitals from './reportWebVitals';

const theme = extendTheme({
  colors, styles, fonts, components: {
    Button: button,
    Input: input,
  }
})

ReactDOM.render(
  <React.StrictMode>
    <ChakraProvider theme={theme}>
      <CSSReset />
      <Fonts />
      <AuthProvider>
        <NotificationProvider>
          <Router>
            <Layout>
              {routes.map(r => (
                <PrivateRoute exact path={r.path} component={r.component} />
              ))}
              <Suspense fallback={<LoadingScreen />}>
                {authRoutes.map(r => (
                  <Route path={r.path} component={r.component} exact={r.exact} />
                ))}
              </Suspense>
            </Layout>
            <Notification />
          </Router>
        </NotificationProvider>
      </AuthProvider>
    </ChakraProvider>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
// reportWebVitals();
