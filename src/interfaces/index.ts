export interface Route {
    path: string;
    component: any;
    exact: boolean;
}

export interface SignUpFormValues {
    email: string;
    name: string; 
    password: string;
    repeatPassword: string;
    tandc: boolean;
    tofs: boolean; 
}

export interface LoginFormValues {
    email: string;
    password: string;
}

export interface Author {
    name: string;
    avatarImgUrl: string;
}

export interface Report {
    id: string;
    company: string;
    companyIcon: string;
    coverImage: string;
    title: string;
    subTitle: string;
    date: string;
    metrics: {
        divYield: number;
        growth: number;
        pbRatio: number;
        peRatio: string;
        stockPrice: number;
    }
    content: string;
    csvUrl?: string;
    authorId?: string;
}

export interface CreateSubscriptionResponse {
    status: string;
    clientSecret: string;
}

export interface ReportListItem {
    reportId: string;
    thumbNailImg: string;
    title: string;
    date: string;
    author: Author;
}

export interface UserProfile {
    email: string,
    photoURL: string;
    name: string;
}

export interface Membership {
    nextBillingDate: string;
    subscriptionDate: string;
    price: string;
    type: string;
}

export interface ExtraUserInfo {
    membership: Membership;
}