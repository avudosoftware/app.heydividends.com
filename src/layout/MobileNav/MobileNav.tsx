import React, { useState } from "react"
import {
  Box, Link, Image, Text, Button
} from "@chakra-ui/react"
import './style.css';
import { Panel } from "../../components/UI";
import MoneyHand from '../../assets/money.svg';
import { useHistory } from 'react-router-dom';

interface SideNavLinkProps {
  text: string;
  active: boolean;
  onClick: () => void;
}

const SideNavLink = ({ text, active, onClick }: SideNavLinkProps) => (
  <p key={`link-${text}`} className={`${active ? 'navLinkSelected' : 'navLink'}`} style={{ marginBottom: 10, color: '#2D3748', paddingLeft: 10, borderRadius: 5, cursor: 'pointer' }} onClick={onClick}>
    <Link
      // aria-current={isActive ? "page" : undefined}
      width="100%"
      py="3"
      rounded="md"
      fontSize={18}
      fontWeight="500"
      letterSpacing={.5}
      color='#2D3748'
      transition="all 0.2s"
      _activeLink={{
        bg: '#E9F2FF',
      }}
      _hover={{
        borderBottom: 'none'
      }}
      key={text}
    >{text}</Link>
  </p>
)

interface SideNavProps {
  isAdmin: boolean;
}

const MobileNav = ({ isAdmin }: SideNavProps) => {
  const history = useHistory()
  const [activeIndex, setActiveIndex] = useState(0)

  const navigate = (index: number, link: string) => {
    history.push(link);
    setActiveIndex(index)
  }

  return (
    <Box
      as="nav"
      aria-label="Main Navigation"
      pos="sticky"
      sx={{
        overscrollBehavior: "contain",
      }}
      top="6.5rem"
      height='100%;'
      w="280px"
      pr="8"
      pb="8"
      overflowY="auto"
      className="sidebar-content"
      flexShrink={0}
    >
      <p style={{ fontWeight: 700, fontSize: 18, color: '#2D3748', letterSpacing: 2.5, paddingLeft: 15 }}>MENU</p>
      <div style={{ marginTop: 10 }}>
        {[{ text: '🔥 Reports', index: 0, link: '/' } , { text: '🎉 Feature Request', index: 2, link: '/feature-request' }].map(o => (
          <SideNavLink key={o.index} text={o.text} onClick={() => navigate(o.index, o.link)} active={activeIndex === o.index} />
        ))}
      </div>
      <p style={{ fontWeight: 700, fontSize: 18, color: '#2D3748', letterSpacing: 2.5, paddingLeft: 15 }}>COMMUNITY</p>
      <div style={{ marginTop: 10 }}>
        <Text px={10}>Coming Soon</Text>
        {/* {[{ text: '📰 Feed', index: 5, link: '/' }, { text: '📈 Stocks', index: 6, link: '/stocks' }].map(o => (
          <SideNavLink key={o.index} text={o.text} onClick={() => navigate(o.index, o.link)} active={activeIndex === o.index} />
        ))} */}
      </div>
      {isAdmin && (
        <div style={{marginTop: 10}}>
          <p style={{ fontWeight: 700, fontSize: 18, color: '#2D3748', letterSpacing: 2.5, paddingLeft: 15 }}>ADMIN</p>
          <div style={{ marginTop: 10 }}>
            {[{ text: '✍️ Write', index: 3, link: '/create-report' }, { text: '📑 View Reports', index: 4, link: '/view-reports' }].map(o => (
              <SideNavLink key={o.index} text={o.text} onClick={() => navigate(o.index, o.link)} active={activeIndex === o.index} />
            ))}
          </div>
        </div>
      )}
      <Panel marginTop={4}>
        <Box textAlign='center' padding={30}>
          <Image alt='money' src={MoneyHand} />
          <Text fontSize={18} fontWeight={600}>Access Everything</Text>
          <Text fontSize={14}>
            View all reports and find potential investment opportunities
          </Text>
          <Button onClick={() => history.push('/subscribe')} _hover={{
            bg: 'linear-gradient(to left, rgba(249, 99, 124, 0.8), rgba(255, 121, 51, 0.8))',
          }} w='100%' marginTop={5}>Go Premium</Button>
        </Box>
      </Panel>
    </Box>
  )
}

export default MobileNav;