import React, { useContext, useEffect, useState } from 'react';
import { useLocation } from 'react-router-dom';
import Header from './Header/Header';
import SideNav from './SideNav/SideNav';
import Content from './Content/Content';
import { Container } from '../components/UI';
import { AuthContext } from '../contexts/auth';
import firebase from 'firebase'
import MobileNav from './MobileNav/MobileNav'
import Logo from '../assets/logo.svg'
import { CloseIcon } from '@chakra-ui/icons';
import { Drawer, DrawerBody, DrawerContent, DrawerHeader, DrawerOverlay, Flex, IconButton, Image } from '@chakra-ui/react';

interface LayoutProps {
  children: any;
}

const Layout = ({ children }: LayoutProps) => {
  const [isAdmin, setIsAdmin] = useState(false);
  const location = useLocation()
  const [mobileNavOpen, openMobileNav] = useState(false);
  const isAuthLocation = (location.pathname === '/login') || (location.pathname === '/signup') || (location.pathname === '/subscribe')
  const user = useContext(AuthContext).currentUser as firebase.User;

  // TODO: Make More efficieant by moving into Auth context
  useEffect(() => {
    if (user) {
      user.getIdTokenResult().then(token => {
        const admin = token.claims.admin || false;
        setIsAdmin(admin)
      })
    }
  }, [user])

  return (
    <>
      {isAuthLocation ? (
        <>
          {children}
        </>
      ) : (
          <>
            <Header
              avatar={user && user.photoURL as string}
              openMobileNav={() => openMobileNav(true)}
            />
            <Container marginTop={20}>
              <SideNav isAdmin={isAdmin} />
              <Content>
                {children}
              </Content>
            </Container>
            <Drawer placement='left' onClose={() => openMobileNav(false)} isOpen={mobileNavOpen}>
              <DrawerOverlay>
                <DrawerContent>
                  <DrawerHeader borderBottomWidth="1px">
                    <Flex>
                      <Image width={170} src={Logo} alt='logo' />
                      <Flex
                        style={{
                          flexGrow: 1,
                          justifyContent: 'flex-end'
                        }}
                      >
                        <IconButton
                          aria-label='close'
                          variant='ghost'
                          icon={<CloseIcon />}
                          onClick={() => openMobileNav(false)}
                        />
                      </Flex>
                    </Flex>
                  </DrawerHeader>
                  <DrawerBody>
                    <MobileNav
                      isAdmin={isAdmin}
                    />
                  </DrawerBody>
                </DrawerContent>
              </DrawerOverlay>
            </Drawer>
          </>
        )}
    </>
  )
}

export default Layout;