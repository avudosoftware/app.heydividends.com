import React from 'react';
import './style.css'
import { isMobile } from 'react-device-detect';
import DesktopHeader from './DesktopHeader/DesktopHeader';
import MobileHeader from './MobileHeader/MobileHeader';

interface HeaderProps {
  avatar: string;
  openMobileNav: () => void;
}

const Header = ({ avatar, openMobileNav }: HeaderProps) => {
  return (
    <>
      {isMobile ? (
        <MobileHeader 
          avatar={avatar}
          openMobileNav={openMobileNav}
        />
      ) : (
          <DesktopHeader avatar={avatar} />
        )}
    </>
  )
}

export default Header;