import React from 'react';
import { chakra, Flex, IconButton } from '@chakra-ui/react';
import { HamburgerIcon } from '@chakra-ui/icons';
import { useViewportScroll } from 'framer-motion';
import Avatar from '../../../components/UI/Avatar/Avatar';
import SettingsDropDown from '../../../components/SettingsDropDown/SettingsDropDown';


interface MobileHeaderProps {
  avatar: string;
  openMobileNav: () => void;
}

const MobileHeader = ({ avatar, openMobileNav }: MobileHeaderProps) => {
  const [y, setY] = React.useState(0)
  const ref = React.useRef<HTMLHeadingElement>()
  const { height = 0 } = ref.current?.getBoundingClientRect() ?? {}
  const { scrollY } = useViewportScroll()
  React.useEffect(() => {
    return scrollY.onChange(() => setY(scrollY.get()))
  }, [scrollY])

  return (
    <chakra.header
      ref={ref as any}
      className='header'
      shadow={y > height ? "sm" : undefined}
      transition="box-shadow 0.2s"
      zIndex={3}
    >
      <Flex w='100%' h="100%" px="6" align="center" justify="space-between">
        <Flex align="center">
          <IconButton 
            icon={<HamburgerIcon color='#2D3748' height={28} 
            />}
            aria-label='menu'
            variant='ghost'
            backgroundColor='transparent !important'
            onClick={openMobileNav}
          />
        </Flex>
        <Flex
          justify="flex-end"
          w="100%"
          maxW="824px"
          align="center"
          color="gray.400"
        >
          <Avatar
            avatarImgUrl={avatar}
          />
          <SettingsDropDown />
        </Flex>
      </Flex>
    </chakra.header>
  )
}


export default MobileHeader;