import React from 'react';
import { chakra, Flex, Link, Image } from '@chakra-ui/react';
import { isMobile } from 'react-device-detect';
import { useViewportScroll } from 'framer-motion';
import Logo from '../../../assets/logo.svg';
import Avatar from '../../../components/UI/Avatar/Avatar';
import SettingsDropDown from '../../../components/SettingsDropDown/SettingsDropDown';


interface DesktopHeaderProps {
  avatar: string;
}

const DesktopHeader = ({ avatar }:DesktopHeaderProps) => {
  const [y, setY] = React.useState(0)
  const ref = React.useRef<HTMLHeadingElement>()
  const { height = 0 } = ref.current?.getBoundingClientRect() ?? {}
  const { scrollY } = useViewportScroll()
  React.useEffect(() => {
    return scrollY.onChange(() => setY(scrollY.get()))
  }, [scrollY])

  return (
    <chakra.header
      ref={ref as any}
      className='header'
      shadow={y > height ? "sm" : undefined}
      transition="box-shadow 0.2s"
      zIndex={3}
    >
      <chakra.div height="4.5rem" mx="auto" maxW="1500px">
        <Flex w='100%' h="100%" px="6" align="center" justify="space-between">
          <Flex align="center">
            {isMobile ? (<div>Mobile</div>) : (<Image alt='logo' src={Logo} />)}
          </Flex>
          <Flex
            justify="flex-end"
            w="100%"
            maxW="824px"
            align="center"
            color="gray.400"
          >
            <Link color='brand.700' fontSize={18} paddingLeft={30} fontWeight={600}>
              Reports
          </Link>
            <Link color='brand.700' fontSize={18} paddingLeft={30} fontWeight={600}>
              Blog
          </Link>
            <Avatar
              avatarImgUrl={avatar}
            />
            <SettingsDropDown />
          </Flex>
        </Flex>
      </chakra.div>
    </chakra.header>
  )
}


export default DesktopHeader;