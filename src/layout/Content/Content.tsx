import React from 'react';
import { Flex } from '@chakra-ui/react';

interface ContentProps {
  children: any
}

const Content = ({ children }: ContentProps) => {
  return (
    <Flex
      w='100%'
    >
      {children}
    </Flex>
  )
}

export default Content;