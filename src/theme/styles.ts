import { ThemeOverride } from "@chakra-ui/react";

const styles =  {
    styles: {
        button: {
            backgroundColor: 'linear-gradient(to left, #F9637C, #FF7933)'
        },
        global: {
            fontFamily: 'Poppins'
        }
    }
} as ThemeOverride

export default styles.styles;