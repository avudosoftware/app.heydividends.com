import { ThemeOverride } from "@chakra-ui/react";

const styles =  {
    fonts: {
        heading: 'Poppins',
        body: 'Poppins',
    }
} as ThemeOverride

export default styles.fonts;