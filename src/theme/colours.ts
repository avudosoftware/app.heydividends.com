const colours =  {
    brand: {
      100: "#E9F2FF", // Light Blue
      200: "#7D7D7D", // default Grey
      300: "rgba(125, 125, 125, 0.6)", // secondary grey
      400: "#A7A7A7", // light grey
      500: '#1ABC9C', // Green
      600: '#70DAC5',
      700: '#2D3748',
      800: '#F94E4E', // Red
    },
}

export default colours;