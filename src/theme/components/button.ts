const Button = {
    // The styles all button have in common
    baseStyle: {
      fontWeight: "bold",
    },
    // Two variants: outline and solid
    variants: {
      solid: {
        bg: "linear-gradient(to left, #F9637C, #FF7933)",
        color: "white",
        _hover : {
            bg: 'linear-gradient(to left, rgba(249, 99, 124, 0.8), rgba(255, 121, 51, 0.8))'
        },
      },
    },
    // The default size and variant values
    defaultProps: {
      size: "md",
    },
    _hover: {
        bg: "linear-gradient(to left, #F9637C, #FF7933)",
    }
  }

  export default Button;