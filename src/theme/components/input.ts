const Input = {
  variants: {
    default: {
      size: "md",
      bg: "#F4F4F4",
      color: "#BFBFBF",
      border: '2px solid #F4F4F4'
    },
  },
  // The default size and variant values
  defaultProps: {
    size: "md",
    variant: "default",
  },
}

export default Input;