import { useState } from 'react';
import { useToast } from '@chakra-ui/react';
import { db } from '../../firebase/firebase';
import { useHistory } from 'react-router-dom';


const useCreateReport = () => {
  const toast = useToast()
  const history = useHistory()
  const [isLoading, setIsLoading] = useState(false);

 
  const createRequest = (request: String) => {
    setIsLoading(true);
    db.collection('requests')
      .doc()
      .set({
        request
      })
      .then(() => {
        setIsLoading(false)
        toast({
          title: `Request submited.`,
          description: 'We will take a look at it and raise the idea with the community',
          duration: 4000,
          isClosable: true,
          status: 'success'
        })
        history.push('/')
      })
      .catch(err => {
        setIsLoading(false)
        toast({
          title: 'Issue submiting your request',
          description: 'Please try again.',
          duration: 4000,
          isClosable: true,
          status: 'error'
        })
      })
  }

  return {
    isLoading,
    createRequest
  }
}

export default useCreateReport;