import { useState } from 'react';
import { functions } from '../../firebase/firebase';
import { CreateSubscriptionResponse } from '../../interfaces';

/**
 * Create a stripe subscription.
 * @param email 
 * @param paymentMethod 
 * @param plan 
 */
const useCreateSubscription = () => {
    const [isLoading, setIsLoading] = useState(false);
    const [error, setError] = useState('');
    const [data, setData] = useState({} as CreateSubscriptionResponse)
    const createSub = functions.httpsCallable('createSubscription');

    const createSubscription = (email: string, paymentMethod: any, plan: 1 | 2) => {
        setIsLoading(true)
        createSub({ email, paymentMethod, plan })
        .then(res => {
            setData(data)
            setIsLoading(false);
        })
        .catch(err => {
            setError(err)
            setIsLoading(false);
        })
    }


    return {
        isLoading,
        error,
        data,
        createSubscription
    }
}

export default useCreateSubscription;