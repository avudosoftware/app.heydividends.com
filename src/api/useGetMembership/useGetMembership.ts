import { useEffect, useState } from 'react';
import { db } from '../../firebase/firebase';
import { Membership } from '../../interfaces';
import { useToast } from '@chakra-ui/react';
import { useHistory } from 'react-router-dom';

/**
 * Retuns a membership of the specified logged in user.
 * @param id id of the report you wish to view.
 */
const useGetMembership = (id: string) => {
  const [isLoading, setIsLoading] = useState(false);
  const [data, setData] = useState({} as Membership)
  const toast = useToast();
  const history = useHistory();

  useEffect(() => {
    (() => {
        setIsLoading(true);
        db.collection('users')
        .doc(`user-${id}`)
        .get()
        .then(respone => {
            const data = respone.data()
            console.log(data)
            setData(data?.membership as Membership);
            setIsLoading(false);
        })
        .catch(() => {
            toast({
                title: 'Error getting your membership details',
                description: 'Please try again later.',
                status: 'error',
                duration: 3000
            })
            history.push('/')
        })
    })()
  }, [id, toast, history])

  return {
    isLoading,
    data
  }
}

export default useGetMembership;