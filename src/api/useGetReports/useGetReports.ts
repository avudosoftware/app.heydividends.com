import { useToast } from '@chakra-ui/react';
import { useEffect, useState } from 'react';
import { db } from '../../firebase/firebase';
import { ReportListItem } from '../../interfaces';


const useGetReports = () => {
  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState('');
  const [data, setData] = useState([{}] as ReportListItem[])
  const toast = useToast();

  useEffect(() => {
    (() => {
      setIsLoading(true);
      db.collection('report-list').get().then(snapshot => {
        const reports = snapshot.docs.map(doc => doc.data());
        setData(reports as ReportListItem[])
        setIsLoading(false)
      }).catch(e => {
        setError('We could not find any reports at this time.')
        setIsLoading(false)
        toast({
          title: 'Error Getting Reports',
          description: 'We could not find any reports at this time.',
          status: 'error',
          duration: 3000
        })
      })
    })()
  }, [toast])

  return {
    isLoading,
    error,
    data
  }
}

export default useGetReports;