import { useContext, useState } from 'react';
import { useToast } from '@chakra-ui/react';
import { Report } from '../../interfaces';
import { db } from '../../firebase/firebase';
import { useHistory } from 'react-router-dom';
import { AuthContext } from '../../contexts/auth';

const useCreateReport = () => {
  const toast = useToast()
  const history = useHistory()
  const { currentUser } = useContext(AuthContext);
  const [isLoading, setIsLoading] = useState(false);
  const [stored, setStored] = useState(false)

  /**
   * Create a report. Parse in a Report object.
   * @param config 
   */
  const createReport = (config: Report) => {
    setIsLoading(true);
    db.collection('reports')
      .doc(config.id)
      .set({
        ...config,
        // Convert JSON to string for storage
        content: JSON.stringify(config.content),
        authorId: currentUser.uid
      })
      .then(() => {
        setStored(true);
        setIsLoading(false)
        toast({
          title: `Created ${config.title}.`,
          description: 'Report created. Your report will soon be available in search results.',
          duration: 4000,
          isClosable: true,
          status: 'success'
        })
        history.push(`/view-report/${config.id}`)
      })
      .catch(err => {
        setIsLoading(false)
        toast({
          title: 'Failed to create report',
          description: 'We could not create your report.',
          duration: 4000,
          isClosable: true,
          status: 'error'
        })
      })
  }

  return {
    isLoading,
    stored,
    createReport
  }
}

export default useCreateReport;