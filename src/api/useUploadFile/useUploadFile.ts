import { useState } from 'react';
import { storage } from '../../firebase/firebase';
import moment from 'moment';

const useUploadFile = () => {
  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState('');
  const [url, setUrl] = useState('')

  /**
   * Upload a file to firebase.
   * @param file The file to be uploaded.
   */
  const uploadFile = (file: File) => {
    setIsLoading(true);
    const filepath = `imgs/${moment()}-${file.name}.${file.type.split('/')[1]}`
    const storageRef = storage.ref(filepath)
    const task = storageRef.put(file)
    task.on('state_changed', (snapshot) => {
      // Add percentage
    },
      (err) => {
        setError(err.message)
      },
      () => {
        setUrl(`heydividends-a4c68.appspot.com/${filepath}`)
        setIsLoading(false);
      }
    )
    return url;
  }

  return {
    isLoading,
    error,
    url,
    uploadFile
  }
}

export default useUploadFile;