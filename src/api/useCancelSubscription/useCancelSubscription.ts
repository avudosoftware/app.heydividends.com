import { useToast } from '@chakra-ui/react';
import { useState } from 'react';
import { useHistory } from 'react-router-dom';
import { functions } from '../../firebase/firebase';

const useCancelSubscription = () => {
  const [loading, setIsLoading] = useState(false);
  const [data, setData] = useState({})
  const cancelSub = functions.httpsCallable('cancelSubscription');
  const toast = useToast();
  const history = useHistory();

  const cancelSubscription = () => {
    setIsLoading(true)
    cancelSub()
      .then(r => {
        setData({ cancled: true })
        setIsLoading(false)
        toast({
          title: 'Membership cancled!',
          description: 'Your membership will not be renewed',
          duration: 3000,
          isClosable: true,
          status: 'success'
        })
        history.push('/')
      })
      .catch(() => {
        toast({
          title: 'There was an issue canceling your membership',
          description: 'Please try again.',
          duration: 3000,
          isClosable: true,
          status: 'error'
        })
      })
  }


  return {
    loading,
    data,
    cancelSubscription
  }
}

export default useCancelSubscription;