import { useEffect, useState } from 'react';
import { db } from '../../firebase/firebase';
import { Report } from '../../interfaces';

/**
 * Retuns a report from the DB that relates to the specidied id.
 * @param id id of the report you wish to view.
 */
const useLoginWithProvider = () => {
  const [isLoading, setIsLoading] = useState(false);
  

 

  return {
    isLoading,
  }
}

export default useLoginWithProvider;