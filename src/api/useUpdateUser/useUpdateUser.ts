import { auth } from '../../firebase/firebase';
import { useState } from 'react';
import { useToast } from '@chakra-ui/react';
import { useHistory } from 'react-router-dom';

interface UpdaterUserVariables {
  name?: string;
  email?: string;
  avatar?: string;
}

const useUpdateUser = () => {
  const [isLoading, setIsLoading] = useState(false);
  const history = useHistory()
  const toast = useToast()

  const updateUser = (updateVariables: UpdaterUserVariables, initialValues: UpdaterUserVariables, pushToSettings?: boolean) => {
    setIsLoading(true);
    // Update email
    if (updateVariables.email && (updateVariables.email !== initialValues.email)) {
      auth.currentUser?.updateEmail(updateVariables.email).then(() => {
        toast({
          title: 'Email updated',
          description: 'Your email has been updated.',
          status: 'success',
          duration: 3000
        })
      }).catch(() => {
        toast({
          title: 'Error updating your email',
          description: 'Please try again later.',
          status: 'error',
          duration: 3000
        })
      })
    }

    // Update name
    if(updateVariables.name && (updateVariables.name !== initialValues.name)) {
      auth.currentUser?.updateProfile({displayName: updateVariables.name}).then(() => {
        toast({
          title: 'Name updated',
          description: 'Your name has been updated.',
          status: 'success',
          duration: 3000
        })
      }).catch(() => {
        toast({
          title: 'Error updating your name',
          description: 'Please try again later.',
          status: 'error',
          duration: 3000
        })
      })
    }

    if(updateVariables.avatar && (updateVariables.avatar !== initialValues.avatar)) {
      auth.currentUser?.updateProfile({photoURL: updateVariables.avatar}).then(() => {
        toast({
          title: 'Avatar updated',
          description: 'Your Avatar has been updated.',
          status: 'success',
          duration: 3000
        })
      }).catch(() => {
        toast({
          title: 'Error updating your email',
          description: 'Please try again later.',
          status: 'error',
          duration: 3000
        })
      })
    }

    setIsLoading(false)
    if (pushToSettings) history.push('/');
  }

  return {
    isLoading,
    updateUser
  }
}

export default useUpdateUser;