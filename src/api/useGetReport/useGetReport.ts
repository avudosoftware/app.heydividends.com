import { useEffect, useState } from 'react';
import { db } from '../../firebase/firebase';
import { Report } from '../../interfaces';

/**
 * Retuns a report from the DB that relates to the specidied id.
 * @param id id of the report you wish to view.
 */
const useGetReport = (id: string) => {
  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState('');
  const [data, setData] = useState(null || {} as Report)

  useEffect(() => {
    (() => {
      setIsLoading(true);
      const getReport = db.collection('reports')
        .doc(id)
        .onSnapshot((snapshot) => {
          const data = snapshot.data();
          if(data) {
            console.log(data, 'data')
            setData(data as Report);
          }
          setIsLoading(false);
        },
          (error) => setError(error.message)
        )
        return () => getReport()
    })()
  }, [id])

  return {
    isLoading,
    error,
    data
  }
}

export default useGetReport;