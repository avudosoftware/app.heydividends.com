import React from 'react';
import { Route } from "../interfaces";

const routes = [
    {
        path: '/',
        component: React.lazy(() => import('../pages/Reports/Reports')),
        exact: true
    },
    {
        path: '/community',
        component: React.lazy(() => import('../pages/Community/Community')),
        exact: true
    },
    {
        path: '/view-report/:id',
        component: React.lazy(() => import('../pages/ViewReport/ViewReport')),
        exact: true
    },
    {
        path: '/subscribe',
        component: React.lazy(() => import('../pages/Subscribe/Subscribe')),
        exact: true
    },
    {
        path: '/create-report',
        component: React.lazy(() => import('../pages/CreateReport/CreateReport')),
        exact: true
    },
    {
        path: '/settings',
        component: React.lazy(() => import('../pages/Settings/Settings')),
        exact: true
    },
    {
        path: '/stocks',
        component: React.lazy(() => import('../pages/CommunityStocks/CommunityStocks')),
        exact: true
    },
    {
        path: '/feature-request',
        component: React.lazy(() => import('../pages/FeatureRequest/FeatureRequest')),
        exact: true
    },
] as Route[]


export default routes;