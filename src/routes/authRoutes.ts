import React from 'react';
import { Route } from "../interfaces";

const authRoutes = [
    {
        path: '/login',
        component: React.lazy(() => import('../pages/Login/Login')),
        exact: true
    },
    {
        path: '/signup',
        component: React.lazy(() => import('../pages/SignUp/SignUp')),
        exact: true
    },
] as Route[]


export default authRoutes;