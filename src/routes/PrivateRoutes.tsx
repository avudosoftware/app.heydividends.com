import React, { Suspense, useContext } from 'react';
import { Route, Redirect } from 'react-router-dom';
import LoadingScreen from '../components/LoadingScreen/LoadingScreen';
import { AuthContext } from '../contexts/auth';


interface PrivateRouteProps {
  component: any;
  [x: string]: any;
}

const PrivateRoute = ({ component: RouteComponent, ...rest }: PrivateRouteProps) => {
  const auth = useContext(AuthContext);

  // TODO: Add auth check here and redirect if the membership flag is checked.

  return (
      <Route
        {...rest}
        render={routeProps => (
            <Suspense fallback={<LoadingScreen />}>
              {auth.currentUser ? (<RouteComponent {...routeProps} />) : (<Redirect to='/login' />)}
            </Suspense>
        )}
      />
  )
}

export default PrivateRoute;