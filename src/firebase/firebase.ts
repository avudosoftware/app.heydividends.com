import firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/firestore';
import 'firebase/functions';
import 'firebase/storage';

const config = {
    apiKey: "AIzaSyDHHoLVZDZwQ58lSQOK6mfv9-ueqY4-3X4",
    authDomain: "heydividends-a4c68.firebaseapp.com",
    projectId: "heydividends-a4c68",
    storageBucket: "heydividends-a4c68.appspot.com",
    messagingSenderId: "162309076992",
    appId: "1:162309076992:web:8bb2db7f7df18f418a80e5",
    measurementId: "G-1RFSDR0MMJ"
}

firebase.initializeApp(config)

export const app = firebase;
export const db = firebase.firestore()
export const auth = firebase.auth()
export const functions = firebase.functions()
export const storage = firebase.storage()