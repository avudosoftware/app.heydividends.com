import React from 'react';
import { Box, SimpleGrid, Image, Flex } from "@chakra-ui/react"
import ChartBackGround from '../assets/chartbg.svg';
import SecondaryLogo from '../assets/logoSec.svg';
import { isMobile } from 'react-device-detect';

interface AuthLayoutProps {
  children: any;
}

const AuthLayout = ({ children }: AuthLayoutProps) => (
  <SimpleGrid display={isMobile ? 'block' : 'grid'} style={{padding: isMobile ? 10 : 'inherit'}} columns={2}>
    <Flex height="100vh" style={{ justifyContent: 'center', alignItems: 'center' }}>
      <Box>
        <Flex style={{ justifyContent: 'center', marginBottom: 20 }}>
          <Image src={SecondaryLogo} alt='secondary' />
        </Flex>
        {children}
      </Box>
    </Flex>
    {!isMobile && (
      <Box bg="#E9F2FF" height="100vh">
        <div style={{ position: 'absolute', bottom: 0 }}>
          <Image src={ChartBackGround} />
        </div>
      </Box>
    )}
  </SimpleGrid>
)

export default AuthLayout;